#!/usr/bin/env python3

def translate_text(target: str, text: str) -> dict:
    """Translates text into the target language.

    Target must be an ISO 639-1 language code.
    See https://g.co/cloud/translate/v2/translate-reference#supported_languages
    """
    from google.cloud import translate_v2 as translate

    translate_client = translate.Client()

    if isinstance(text, bytes):
        text = text.decode("utf-8")

    # Text can also be a sequence of strings, in which case this method
    # will return a sequence of results for each text.
    result = translate_client.translate(text, target_language=target)

    #print("Text: {}".format(result["input"]))
    #print("Translation: {}".format(result["translatedText"]))
    #print("Detected source language: {}".format(result["detectedSourceLanguage"]))

    return result

def number_to_words(number: int):
	import inflect
	p = inflect.engine()

	return p.number_to_words(number)

import random, sys

number_digits=random.randrange(int(sys.argv[1]))
number_words=number_to_words(number_digits)
print(number_digits)
input("...")
swedish=translate_text("sv", number_words)
print("{}".format(swedish["translatedText"]))
